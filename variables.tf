variable "default_tags" {}
variable "use_hetzner_dns" {
  type = bool
  description = "Whether or not to use Hetzner DNS"
}
variable "domain_zone" {
  description = "Required if using Hetzner DNS"
  default = ""
}

variable "hostname" {
  description = "FQDN of the API Server"
}

variable "name_prefix" {
  type = string
  description = "Prefix to use for the names of the servers"
}
variable "network" {
  type = map(object({
    cidr    = string
    subnets = list(map(any))
  }))
}
variable "vpc_id" {}
variable "masters" {
  default = 1
}
variable "masters_instance_type" {
  default = "cx21"
}
variable "workers" {
  default = 0
}
variable "workers_instance_type" {
  default = "cx21"
}
variable "ssh_keys" {
  type = list(string)
}
variable "hcloud_ccm_token" {}
variable "hcloud_ccm_network_name" {}