locals {
  kubeconfig = base64decode(jsondecode(ssh_resource.example.result).kubeconfig)
}

output "masters_external_ips" {
  value = hcloud_server.k3s_masters[*].ipv4_address
}

output "kubeconfig" {
  value     = replace(local.kubeconfig, "/server:.*6443/", format("server: https://%s:6443", var.hostname))
  sensitive = true
}

