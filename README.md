<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | >= 1.32.1 |
| <a name="requirement_hetznerdns"></a> [hetznerdns](#requirement\_hetznerdns) | >= 1.1.1 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 3.1.0 |
| <a name="requirement_ssh"></a> [ssh](#requirement\_ssh) | >= 1.0.1 |
| <a name="requirement_time"></a> [time](#requirement\_time) | >= 0.7.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_hcloud"></a> [hcloud](#provider\_hcloud) | 1.32.2 |
| <a name="provider_hetznerdns"></a> [hetznerdns](#provider\_hetznerdns) | 2.1.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.1.0 |
| <a name="provider_ssh"></a> [ssh](#provider\_ssh) | 1.0.1 |
| <a name="provider_time"></a> [time](#provider\_time) | 0.7.2 |
| <a name="provider_tls"></a> [tls](#provider\_tls) | 3.1.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [hcloud_load_balancer.control_plane](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/load_balancer) | resource |
| [hcloud_load_balancer_network.srvnetwork](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/load_balancer_network) | resource |
| [hcloud_load_balancer_service.control_plane](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/load_balancer_service) | resource |
| [hcloud_load_balancer_target.control_plane_targets](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/load_balancer_target) | resource |
| [hcloud_server.k3s_masters](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/server) | resource |
| [hcloud_server.k3s_workers](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/server) | resource |
| [hcloud_ssh_key.provisioning_ssh_key](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/ssh_key) | resource |
| [hetznerdns_record.djms_stack](https://registry.terraform.io/providers/timohirt/hetznerdns/latest/docs/resources/record) | resource |
| [random_password.server_token](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [ssh_resource.example](https://registry.terraform.io/providers/loafoe/ssh/latest/docs/resources/resource) | resource |
| [time_sleep.wait_30_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |
| [tls_private_key.custom_key](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key) | resource |
| [hcloud_image.rocky](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/data-sources/image) | data source |
| [hetznerdns_zone.dns](https://registry.terraform.io/providers/timohirt/hetznerdns/latest/docs/data-sources/zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_default_tags"></a> [default\_tags](#input\_default\_tags) | n/a | `any` | n/a | yes |
| <a name="input_domain_zone"></a> [domain\_zone](#input\_domain\_zone) | Required if using Hetzner DNS | `string` | `""` | no |
| <a name="input_hcloud_ccm_network_name"></a> [hcloud\_ccm\_network\_name](#input\_hcloud\_ccm\_network\_name) | n/a | `any` | n/a | yes |
| <a name="input_hcloud_ccm_token"></a> [hcloud\_ccm\_token](#input\_hcloud\_ccm\_token) | n/a | `any` | n/a | yes |
| <a name="input_hostname"></a> [hostname](#input\_hostname) | FQDN of the API Server | `any` | n/a | yes |
| <a name="input_masters"></a> [masters](#input\_masters) | n/a | `number` | `1` | no |
| <a name="input_masters_instance_type"></a> [masters\_instance\_type](#input\_masters\_instance\_type) | n/a | `string` | `"cx21"` | no |
| <a name="input_name_prefix"></a> [name\_prefix](#input\_name\_prefix) | Prefix to use for the names of the servers | `string` | n/a | yes |
| <a name="input_network"></a> [network](#input\_network) | n/a | `any` | n/a | yes |
| <a name="input_ssh_keys"></a> [ssh\_keys](#input\_ssh\_keys) | n/a | `list(string)` | n/a | yes |
| <a name="input_use_hetzner_dns"></a> [use\_hetzner\_dns](#input\_use\_hetzner\_dns) | Whether or not to use Hetzner DNS | `bool` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | n/a | `any` | n/a | yes |
| <a name="input_workers"></a> [workers](#input\_workers) | n/a | `number` | `0` | no |
| <a name="input_workers_instance_type"></a> [workers\_instance\_type](#input\_workers\_instance\_type) | n/a | `string` | `"cx21"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_kubeconfig"></a> [kubeconfig](#output\_kubeconfig) | n/a |
| <a name="output_masters_external_ips"></a> [masters\_external\_ips](#output\_masters\_external\_ips) | n/a |
<!-- END_TF_DOCS -->