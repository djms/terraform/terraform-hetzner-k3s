terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">= 1.32.1"
    }
    hetznerdns = {
      source  = "timohirt/hetznerdns"
      version = ">= 1.1.1"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.1.0"
    }
    ssh = {
      source  = "loafoe/ssh"
      version = ">= 1.0.1"
    }
    time = {
      source  = "hashicorp/time"
      version = ">= 0.7.2"
    }
  }
}
