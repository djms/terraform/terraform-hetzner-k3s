locals {
  all_sn = length(var.network.subnets)
}

resource "hcloud_server" "k3s_masters" {
  count       = var.masters
  name        = format("%s-master-%02d", var.name_prefix, count.index + 1)
  image       = data.hcloud_image.rocky.name
  server_type = var.masters_instance_type
  location    = var.network.subnets[count.index % local.all_sn].az
  ssh_keys = concat(var.ssh_keys, [
    hcloud_ssh_key.provisioning_ssh_key.id
  ])
  network {
    network_id = var.vpc_id
    ip         = cidrhost(var.network.subnets[count.index % local.all_sn].cidr, count.index + 1)
  }

  user_data = templatefile("${path.module}/templates/userdata.sh.tpl", {
    cluster_init            = count.index == 0 ? true : false
    server_token            = random_password.server_token.result
    local_ip                = cidrhost(var.network.subnets[count.index % local.all_sn].cidr, count.index + 1)
    server_type             = "server"
    server_url              = format("https://%s:6443", cidrhost(var.network.subnets[0].cidr, 1))
    fqdn                    = var.hostname
    hcloud_ccm_token        = var.hcloud_ccm_token
    hcloud_ccm_network_name = var.hcloud_ccm_network_name
    hcloud_ccm_manifest     = file("${path.module}/templates/ccm.yaml")
  })

  labels = merge(var.default_tags, {
    controlplane = "true"
  })
}

resource "hcloud_load_balancer" "control_plane" {
  name               = format("%s-controlplane-lb", var.name_prefix)
  load_balancer_type = "lb11"
  network_zone       = "eu-central"

  labels = merge(var.default_tags, {
    controlplane = "true"
  })

  depends_on = [
    hcloud_server.k3s_masters
  ]
}

resource "hcloud_load_balancer_service" "control_plane" {
  load_balancer_id = hcloud_load_balancer.control_plane.id
  protocol         = "tcp"
  listen_port      = 6443
  destination_port = 6443
}

resource "hcloud_load_balancer_network" "srvnetwork" {
  load_balancer_id = hcloud_load_balancer.control_plane.id
  network_id       = var.vpc_id
}

resource "hcloud_load_balancer_target" "control_plane_targets" {
  type             = "label_selector"
  label_selector   = "controlplane=true"
  load_balancer_id = hcloud_load_balancer.control_plane.id
  use_private_ip   = true

  depends_on = [
    hcloud_load_balancer.control_plane
  ]
}