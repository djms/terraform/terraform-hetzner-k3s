resource "hcloud_server" "k3s_workers" {
  count       = var.workers
  name        = format("%s-worker-%02d", var.name_prefix, count.index + 1)
  image       = data.hcloud_image.rocky.name
  server_type = var.workers_instance_type
  location    = var.network.subnets[count.index % local.all_sn].az
  ssh_keys = concat(var.ssh_keys, [
    hcloud_ssh_key.provisioning_ssh_key.id
  ])
  network {
    network_id = var.vpc_id
    ip         = cidrhost(var.network.subnets[count.index % local.all_sn].cidr, count.index + 21)
  }

  user_data = templatefile("${path.module}/templates/userdata-worker.sh.tpl", {
    server_token = random_password.server_token.result
    server_url   = format("https://%s:6443", var.hostname)
    local_ip     = cidrhost(var.network.subnets[count.index % local.all_sn].cidr, count.index + 21)
  })

  labels = merge(var.default_tags, {
    cluster = var.name_prefix
  })
}