data "hetznerdns_zone" "dns" {
  count = var.use_hetzner_dns ? 1 : 0
  name = var.domain_zone
}

resource "hetznerdns_record" "djms_stack" {
  count = var.use_hetzner_dns ? 1 : 0
  zone_id = data.hetznerdns_zone.dns[0].id
  name    = split(var.domain_zone, var.hostname)[0]
  value   = hcloud_load_balancer.control_plane.ipv4
  type    = "A"
  ttl     = 60
}