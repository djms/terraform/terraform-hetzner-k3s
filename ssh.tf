resource "tls_private_key" "custom_key" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "hcloud_ssh_key" "provisioning_ssh_key" {
  name       = format("%s-key", var.name_prefix)
  public_key = tls_private_key.custom_key.public_key_openssh
}

resource "time_sleep" "wait_30_seconds" {
  depends_on = [
    hcloud_server.k3s_masters
  ]
  triggers = {
    server_id = hcloud_server.k3s_masters[0].id
  }
  create_duration = "60s"
}


resource "ssh_resource" "example" {
  host        = hcloud_server.k3s_masters[0].ipv4_address
  user        = "root"
  private_key = tls_private_key.custom_key.private_key_pem

  file {
    content     = file("${path.module}/templates/get-kubeconfig.sh")
    destination = "/root/get-kubeconfig.sh"
    permissions = "0700"
  }

  commands = [
    "/root/get-kubeconfig.sh",
  ]

  depends_on = [time_sleep.wait_30_seconds]
}

