#!/bin/bash -xe

yum update -y
yum install -y tar curl wget gzip net-tools

curl -sfL https://get.k3s.io -o install.sh
chmod +x install.sh

# head -n3 /etc/resolv.conf > /etc/resolv.conf

export INSTALL_K3S_EXEC=${server_type} 
export INSTALL_K3S_VERSION="v1.22.3+k3s1"
export K3S_TOKEN=${server_token}

export EXT_IP=$(curl -s http://169.254.169.254/hetzner/v1/metadata/public-ipv4)

./install.sh \
   --node-ip "${local_ip}" \
   --node-external-ip "$${EXT_IP}" \
   --bind-address "${local_ip}" \
   --tls-san ${fqdn} \
   --advertise-address "${local_ip}" \
   --disable local-storage \
   --disable-cloud-controller \
   --disable traefik \
   --disable servicelb \
   --kubelet-arg cloud-provider=external \
   --secrets-encryption \
   %{ if cluster_init }--cluster-init %{ else } --server ${server_url} %{ endif }

echo "alias k='kubectl' >> /root/.bashrc"

%{ if cluster_init }
cat <<EOF > /tmp/ccm.yaml
${hcloud_ccm_manifest}
EOF

kubectl -n kube-system create secret generic hcloud --from-literal=token=${hcloud_ccm_token} --from-literal=network=${hcloud_ccm_network_name}
kubectl -n kube-system create secret generic hcloud-csi --from-literal=token=${hcloud_ccm_token}
kubectl apply -f /tmp/ccm.yaml 
kubectl apply -f https://raw.githubusercontent.com/hetznercloud/csi-driver/v1.6.0/deploy/kubernetes/hcloud-csi.yml
%{ endif }