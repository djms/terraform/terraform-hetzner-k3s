#!/bin/bash

KUBECONFIG="/etc/rancher/k3s/k3s.yaml"

while [[ ! -f ${KUBECONFIG} ]]; do

  echo "Waiting for stuff to happen" >&2
  sleep 5

done

KUBECONFIG_B64=$(cat ${KUBECONFIG} | base64 -w0)


echo -n "{\"kubeconfig\":\"${KUBECONFIG_B64}\"}"