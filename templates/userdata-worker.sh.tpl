#!/bin/bash -xe

yum update -y
yum install -y tar curl wget gzip net-tools

curl -sfL https://get.k3s.io -o install.sh
chmod +x install.sh

# head -n3 /etc/resolv.conf > /etc/resolv.conf

export INSTALL_K3S_VERSION="v1.22.3+k3s1"
export K3S_TOKEN=${server_token}
export K3S_URL=${server_url}
export EXT_IP=$(curl -s http://169.254.169.254/hetzner/v1/metadata/public-ipv4)

./install.sh \
   --node-ip "${local_ip}" \
   --kubelet-arg 'cloud-provider=external' \
   --node-external-ip "$${EXT_IP}"

echo "alias k='kubectl' >> /root/.bashrc"