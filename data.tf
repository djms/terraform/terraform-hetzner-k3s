data "hcloud_image" "rocky" {
  name        = "rocky-8"
  most_recent = true
}

resource "random_password" "server_token" {
  length           = 16
  special          = true
  override_special = "_%@"
}